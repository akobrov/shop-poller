﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BarboraPollerService.Helpers
{
    public static class ForEachExtension
    {
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }
    }
}
