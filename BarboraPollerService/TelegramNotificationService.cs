﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BarboraPollerService.Models;
using Microsoft.Extensions.Configuration;
using Telegram.Bot;

namespace BarboraPollerService
{
    public interface ITelegramNotificationService
    {
        Task<bool> SendNotification(string content);
    }
    public class TelegramNotificationService : ITelegramNotificationService
    {
        private TelegramConfig _telegramConfig;
        private readonly TelegramBotClient _botClient;
        public TelegramNotificationService(IConfiguration config)
        {
            _telegramConfig = config.GetSection("Telegram").Get<TelegramConfig>();
            _botClient = new TelegramBotClient(_telegramConfig.ApiKey);
        }

        public async Task<bool> SendNotification(string content)
        {
            content = $"Barbora schedule {Environment.NewLine} {content}";
            await _botClient.SendTextMessageAsync(_telegramConfig.ChatId, content);
            return true;
        }
    }
}
