﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace BarboraPollerService.Models
{
    public class BarboraScheduleResponse
    {
        public Deliveries[] Deliveries { get; set; }
        public int ReservationValidForSeconds { get; set; }
        public string[] Messages { get; set; }
    }

    public class Deliveries
    {
        public string Title { get; set; }
        public Params Params { get; set; }
    }

    public class Params
    {
        public Matrix[] Matrix { get; set; }
    }

    public class Matrix
    {
        public string Id { get; set; }
        public bool IsExpressDelivery { get; set; }
        public string Day { get; set; }
        public string DayShort { get; set; }
        public BarboraHours[] Hours { get; set; }
            
    }

    public class BarboraHours
    {
        public string Id { get; set; }
        public string DeliveryTime { get; set; }
        public string Hour { get; set; }
        public decimal Price { get; set; }
        public bool Available { get; set; }
        public bool IsUnavailableAlcSellingTime { get; set; }
        public bool IsUnavailableAlcOrEnergySelling { get; set; }
        public bool IsLockerOrPup { get; set; }
        public decimal SalesCoefficient { get; set; }
        public string DeliveryWave { get; set; }
        public int PickingHour { get; set; }
        public string ChangeTimeslotShop { get; set; }
    }
}
