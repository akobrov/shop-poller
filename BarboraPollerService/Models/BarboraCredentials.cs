﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BarboraPollerService.Models
{
    public class BarboraCredentials
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; } = false;
    }
}
