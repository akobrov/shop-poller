﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BarboraPollerService.Models
{
    public class TelegramConfig
    {
        public string ApiKey { get; set; }
        public string ChatId { get; set; }
    }
}
