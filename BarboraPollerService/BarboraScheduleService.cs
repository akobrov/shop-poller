﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using BarboraPollerService.Helpers;
using BarboraPollerService.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;

namespace BarboraPollerService
{
    public interface IBarboraClient
    {
        public Task<BarboraScheduleResponse> GetSchedule();
        public Task<List<string>> GetScheduleAvailability();
    }

    public class BarboraScheduleService : IBarboraClient, IDisposable
    {
        #region Constants

        private const string LoginUrl = "https://www.barbora.lt/api/eshop/v1/user/login";
        private const string DeliveriesUrl = "https://www.barbora.lt/api/eshop/v1/cart/deliveries";
        #endregion

        private readonly ILogger<BarboraScheduleService> _logger;
        private readonly HttpClient _client;
        private readonly CookieContainer _cookieContainer;
        private readonly IConfiguration _config;
        private BarboraCredentials _barboraCredentials;
        private bool _isLoggedIn = false;

        public BarboraScheduleService(ILogger<BarboraScheduleService> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            RefreshConfiguration();
            _cookieContainer = new CookieContainer();
            var regionCookie = new Cookie("region", "barbora.lt", "/", "barbora.lt");
            _cookieContainer.Add(regionCookie);
            var clientHandler = new HttpClientHandler() { CookieContainer = _cookieContainer };
            _client = new HttpClient(clientHandler);
            _logger.LogInformation("Client created.");
        }

        private async Task<bool> DoLogin()
        {
            _logger.LogInformation("Login called!");

            // set login form parameters
            var loginDetails = new List<KeyValuePair<string, string>>();
            loginDetails.Add(new KeyValuePair<string, string>("email", _barboraCredentials.Login));
            loginDetails.Add(new KeyValuePair<string, string>("password", _barboraCredentials.Password));
            loginDetails.Add(new KeyValuePair<string, string>("rememberMe", _barboraCredentials.RememberMe.ToString()));

            // set hardcoded API key
            var request = new HttpRequestMessage(HttpMethod.Post, LoginUrl) { Content = new FormUrlEncodedContent(loginDetails) };
            request.Headers.Add("Authorization", "Basic YXBpa2V5OlNlY3JldEtleQ==");

            //execute
            var result = await _client.SendAsync(request);
            if (result.StatusCode == HttpStatusCode.OK)
            {

                _logger.LogInformation($"Successfully logged in as {_barboraCredentials.Login}");
                _isLoggedIn = true;
            }
            else
            {
                _logger.LogWarning(result.ToString());
                _isLoggedIn = false;
            }

            return _isLoggedIn;
        }

        public async Task<BarboraScheduleResponse> GetSchedule()
        {
            try
            {
                //make the call
                var request = new HttpRequestMessage(HttpMethod.Get, DeliveriesUrl) { };
                request.Headers.Add("Authorization", "Basic YXBpa2V5OlNlY3JldEtleQ==");
                var requestResult = await _client.SendAsync(request);

                if (!requestResult.IsSuccessStatusCode)
                {
                    if (requestResult.StatusCode == HttpStatusCode.Forbidden ||
                        requestResult.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        await DoLogin();
                        return default(BarboraScheduleResponse);
                    }

                    _logger.LogError($"Unexpected response: {requestResult.ToString()}");
                    throw new Exception("Unexpected result returned from Barbora");
                }

                //Retry policy
                var retryPolicy = Policy
                    .Handle<Exception>()
                    .WaitAndRetryAsync(
                        4,
                        i => TimeSpan.FromSeconds(2), 
                        (ex,timespan) =>
                        {
                            _logger.LogInformation($"Retry policy hit: {ex}"); } 
                        );

                var result = await retryPolicy.ExecuteAsync (async () =>
                {
                    Console.WriteLine("\r\nExecuting request...");
                    var responseContent = await requestResult.Content.ReadAsStringAsync();
                    // parse it
                    var schedule = JsonConvert.DeserializeObject<BarboraScheduleResponse>(responseContent);

                    _logger.LogInformation("Schedule GET: success.");
                    return schedule;
                });

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed! {e}");
                throw e;
            }
        }

        public async Task<List<string>> GetScheduleAvailability()
        {
            _logger.LogInformation("Process called!");

            if (!_isLoggedIn)
            {
                await DoLogin();

                if (!_isLoggedIn)
                {
                    throw new ApplicationException("Failed to login!");
                }
            }

            var schedule = await GetSchedule();
            List<string> availableHours = new List<string>();
            var paramsMatrix = schedule?.Deliveries.First().Params.Matrix;
            if (paramsMatrix != null)
                foreach (var entry in paramsMatrix)
                {
                    entry.Hours.Where(h => h.Available)
                        .ForEach(h => { availableHours.Add($"Available - {entry.Day} - {h.Hour}"); });
                }


            return availableHours;
        }

        #region helpers/dispose
        private void RefreshConfiguration()
        {
            _barboraCredentials = _config.GetSection("BarboraCredentials").Get<BarboraCredentials>();
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
        #endregion
    }
}
