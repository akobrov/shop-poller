using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BarboraPollerService
{
    public class BarboraTimedWorker : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<BarboraTimedWorker> _logger;
        private readonly IConfiguration _config;
        private Timer _timer;

        private readonly IBarboraClient _barboraClient;
        private readonly ITelegramNotificationService _telegramService;

        public BarboraTimedWorker(ILogger<BarboraTimedWorker> logger, IConfiguration config, IBarboraClient barboraClient, ITelegramNotificationService telegramService)
        {
            _logger = logger;
            _config = config;
            _barboraClient = barboraClient;
            _telegramService = telegramService;

        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");

            var pollDelay = Int32.Parse(_config["PollFrequencyInMinutes"]);

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(pollDelay));

            return Task.CompletedTask;
        }

        private async void  DoWork(object state)
        {
            var count = Interlocked.Increment(ref executionCount);

            _logger.LogInformation(
                "Timed Hosted Service is working. Count: {Count}", count);

            var availableSchedule = _barboraClient.GetScheduleAvailability().Result;
            _logger.LogInformation($"Available schedule:{Environment.NewLine}{string.Join(Environment.NewLine, availableSchedule.ToArray())}");

            if (!availableSchedule.Any()) return;
            await _telegramService.SendNotification(string.Join(Environment.NewLine, availableSchedule.ToArray()));
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
